#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../partie1/saeP1.h"
#include "saeAnnexe.h"

void menu(int CandidOpen){
    printf("#--------------------------------------------------------------------#\n");
    printf("|                                                                    |\n");
    printf("|                        Menu de la SAE S1.02                        |\n");
    printf("|                                                                    |\n");
    printf("#--------------------------------------------------------------------#\n\n");
    printf("Codes pour accéder aux différents profils :\n\n");
    printf("C : Consultation.\n");
    printf("A : Administrateur.\n");
    if (CandidOpen) printf("R : Responsable de Département.\n");
    printf("S : Sauvegarder.\n\n");
    printf("Q : Quitter.\n\n");
    printf("#--------------------------------------------------------------------#\n\n");
    printf("Choisissez votre session : ");
}

int loadIUT(VilleIUT** tiut){
    FILE* f=fopen("src/DataBase/Etudiants.bin","rb");
    if(f==NULL){
        perror("fopen");
        exit(errno);
    }
    MaillonDep* m;
    int tlogi;
    fread(&tlogi,sizeof(int),1,f);
    for(int i=0; i<tlogi; i++){
    	tiut[i]=(VilleIUT*)malloc(sizeof(VilleIUT));
    	if(f==NULL){
			perror("fopen");
			exit(errno);
		}
        fread(tiut[i]->Ville,sizeof(char),31,f);
    }
    for(int i=0; i<tlogi; i++){
        int nbIUTs;
        fread(&nbIUTs,sizeof(int),1,f);
        if(nbIUTs){
            tiut[i]->ldept=(MaillonDep*)malloc(sizeof(MaillonDep));
            if(tiut[i]->ldept==NULL){
                perror("malloc");
                exit(errno);
            }
        }
        m=tiut[i]->ldept;
        for(int j=0; j<nbIUTs; j++){
            fread(m->departement,sizeof(char),31,f);
            fread(&m->nbp,sizeof(int),1,f);
            fread(m->resp,sizeof(char),51,f);
            if(j<nbIUTs-1){
                m->suivant=(MaillonDep*)malloc(sizeof(MaillonDep));
                if(m->suivant==NULL){
                    perror("malloc");
                    exit(errno);
                }
                m=m->suivant;
            }
        }
    }
    return tlogi;
}

void saveIUT(VilleIUT** tiut, int tlogi){
    FILE* f=fopen("src/DataBase/Etudiants.bin","wb");
    if(f==NULL){
        perror("fopen");
        exit(errno);
    }
    fwrite(&tlogi,sizeof(int),1,f);
    for(int i=0; i<tlogi; i++){
        fwrite(tiut[i]->Ville,sizeof(char),31,f);
    }
    MaillonDep* m;
    for(int i=0; i<tlogi; i++){
        int nbIUTs=longueurListe(tiut[i]->ldept);
        fwrite(&nbIUTs,sizeof(int),1,f);
        m=tiut[i]->ldept;
        for(int j=0; j<nbIUTs; j++){
            fwrite(m->departement,sizeof(char),31,f);
            fwrite(&m->nbp,sizeof(int),1,f);
            fwrite(m->resp,sizeof(char),51,f);
            m=m->suivant;
        }
    }
    printf("Les IUTs enregistrés ont bien étés sauvegardés.\n");
}

int rechercheIUT(VilleIUT* tab[],int lTab,char mot[]){
    if(!lTab){
        return -1;
    }
    for(int i=0; i<lTab;i++){
        if(!strcmp(tab[i]->Ville,mot)) return i;
    }
    return -1;
}

void tabcpy(int tabS[], int tabD[], int tlogi){
    for(int i=0; i<tlogi; i++){
        tabD[i]=tabS[i];
    }
}

//Listes chainées
int existeDep(MaillonDep* liste, char* dep){
    MaillonDep* m=liste;
    if(!strcmp(m->departement,dep)){
        return 1;
    }
    while(m->suivant!=NULL){
        printf("%s, %s\n",m->departement,dep);
        if(!strcmp(m->departement,dep)){
            return 1;
        }
        m=m->suivant;
    }
    return 0;
}

int longueurListe(MaillonDep* liste){
    MaillonDep* m=liste;
    int i=0;
    while(m!=NULL){
        m=m->suivant;
        i++;
    }
    return i;
}

void reset(void){
  char capt;
  printf("\n\nAppuyez sur entrer pour continuer :\n");
  scanf("%*c%c",&capt);
  system("clear");
}
