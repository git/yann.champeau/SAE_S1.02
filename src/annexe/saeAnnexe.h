//Initialisation de fonctions :

/**
*\file saeAnnexe.h
*\brief Liste des fonctions générales utilisées dans la plupart des .c.
*\author Van Brabandt.J Champeau.Y
*\date 13 janvier 2023
*
*Fichier regroupant les prototypes des fonctions générales utilisées par l'application.
*/



/**
* \brief affiche le menu principal de l'application.
* \param [in] CandidOpen Entier contenant 1 si la phase de canditatures est ouverte, 0 sinon.
*/
void menu(int CandidOpen);
    /*
    Affiche le menu
    Entrée : 
        Booléen notifiant l'ouverture ou non des candidatures CandidOpen;
    Sortie : Aucune
    */



/**
* \brief fonction chargeant le fichier des IUTs. 
* \param [out] tiut Tableau contenant les IUTs.
* \return le nombre d'éléments chargés dans le tableau.
*/
int loadIUT(VilleIUT** tiut);
    /*
    Charge le tableau tiut
    Entrée : 
        tableau des iuts tiuts
    Sortie : 
        taille logique du tableau
    */



/**
* \brief sauvegarde le tableau d'IUTs dans le fichier.
* \param [in] tiut Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void saveIUT(VilleIUT** tiut, int tlogi);
    /*
    Sauvegarde le tableau iut dans un fichier
    Entrée : 
        tableau des iuts tiut
        taille logique du tableau tlogi;
    Sortie : Aucune
    */



/**
* \brief recherche un IUT dans le tableau d'IUTs.
* \param [in] tab Tableau contenant les IUTs.
* \param [in] lTab taille logique du tableau.
* \param [in] mot nom de l'IUT à chercher.
* \return -1 en cas d'erreur ou si l'IUT n'est pas trouvé, son indice dans le tableau sinon.
*/
int rechercheIUT(VilleIUT* tab[],int lTab,char mot[]);
    /*
    Cherche l'ID d'un IUT et le renvoie
    Entrée : 
        tableau des iuts tab;
        taille logique du tableau tlogi;
        mot recherché mot;
    Sortie : 
        Position de l'IUT recherché
    */



/**
* \brief recopie un tableau d'entiers.
* \param [in] tab Tableau à copier.
* \param [in] tlogi taille logique du tableau.
* \return la copie du tableau.
*/
void tabcpy(int tabS[], int tabD[], int tlogi);

//Fonctions relatives aux listes chaînées :

/**
* \brief recherche si un département existe dans la liste de départements.
* \param [in] liste liste des départements.
* \param [in] dep nom du département à chercher.
* \return 1 si le département existe, 0 sinon.
*/
int existeDep(MaillonDep* liste, char* dep);
    /*
    Regarde si un département existe dans la liste chainé
    Entrée : 
        liste chainé liste;
        département recheché dep;
    Sortie : 
        Booléen d'existence.
    */



/**
* \brief calcule la longueur d'une liste de départements.
* \param [in] liste liste de départements.
* \return la longueur de \b liste .
*/
int longueurListe(MaillonDep* liste);
    /*
    Renvoie la longueur de la liste
    Entrée : 
        liste chainé liste;
    Sortie : 
        Longueur de la liste.
    */
//-----------------------------------------------//


/**
* \brief réinitialise l'affichage.
*/
void reset(void);
    /*
    Permet d'effacer le menu actuel.
    Entrée : Aucune
    Sortie : Aucune
    */