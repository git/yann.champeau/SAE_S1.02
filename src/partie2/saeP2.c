#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../partie1/saeP1.h"
#include "../annexe/saeAnnexe.h"

candidat** LoadCandid(int* CandidOpen, int* nbcandid)
{
//J'ouvre le fichier
    FILE* file;
    if((file=fopen("src/DataBase/candid.bin","rb"))==NULL)
    {
        perror("Erreur : ");
        exit(1);
    }
    fread(CandidOpen,sizeof(int),1,file);
//Lire la première ligne pr savoir combien d'étudiant je vais lire
    fread(nbcandid,sizeof(int),1,file);
    candidat** Tabcandidat=(candidat**) malloc (sizeof(candidat*)*(*nbcandid));
    if(Tabcandidat==NULL){
        perror("malloc");
        exit(errno);
    }
//Tant que j'ai des étudiants à lire :
    for(int i=0;*nbcandid>i;++i)
    {
//Son numéro, Son nom, Son prenom, liste de note`
        Tabcandidat[i]=(candidat*)malloc(sizeof(candidat));
        if(Tabcandidat[i]==NULL){
            perror("malloc");
            exit(errno);
        }
        fread(&Tabcandidat[i]->numcandid,sizeof(int),1,file);
        fread(Tabcandidat[i]->surname,sizeof(char)*31,1,file);
        fread(Tabcandidat[i]->name,sizeof(char)*31,1,file);
        fread(Tabcandidat[i]->note,sizeof(float)*4,1,file);
//Son nb de choix
        fread(&Tabcandidat[i]->nbchoix,sizeof(int),1,file);
//Tant que j'ai pas fait tout ces choix :
        for(int y=0;y<Tabcandidat[i]->nbchoix;++y)
        {
//Je lis La ville, le département, la décision du dep, la validation du candidat
            voeu* voeu_ajout=(voeu*)malloc(sizeof(voeu));
            fread(voeu_ajout->ville,sizeof(char)*31,1,file);
            fread(voeu_ajout->dep,sizeof(char)*31,1,file);
            fread(&voeu_ajout->ddep,sizeof(int),1,file);
            fread(&voeu_ajout->vcand,sizeof(int),1,file);
            Tabcandidat[i]->TabVoeu[i]=voeu_ajout;
        }
    }
//Je sors
    return Tabcandidat;
}

void SaveCandid(int nbcandidat,candidat *tcandidat[],int CandidOpen)
{
//J'ouvre le fichier
    FILE* file;
    if((file=fopen("src/DataBase/candid.bin","wb"))==NULL)
    {
        perror("Erreur : ");
        exit(1);
    }
    fwrite(&CandidOpen,sizeof(int),1,file);
//J'écris le nombre d'étudiant que je rentres
    fwrite(&nbcandidat,sizeof(int),1,file);
//J'écris tout mes étudiants
    fwrite(tcandidat,sizeof(candidat),nbcandidat,file);
    printf("Sauvegarde réussit");
}

void RechercheCandidat(int ID, int nbcandidat,candidat *tcandidat[])
{
    for(int i=0;i<nbcandidat;++i)
    {
        if(i==nbcandidat)
        {
            printf("Candidat Trouvé :\n\
            Numéro : %d\n\
            Nom : %s\n\
            Prenom : %s\n\
            Note de mathématique : %f\n\
            Note de français : %f\n\
            Note d'anglais : %f\n\
            Note de spécialité : %f\n\
            Nombre de voeu : %d\n\
            Voeu :\n",tcandidat[i]->numcandid,tcandidat[i]->surname,tcandidat[i]->name,\
            tcandidat[i]->note[0],tcandidat[i]->note[1],tcandidat[i]->note[2],\
            tcandidat[i]->note[3],tcandidat[i]->nbchoix);
            for(int y=0;y<tcandidat[i]->nbchoix;++y)
            {
                printf("Ville : %s\n\
                Departement : %s\n\
                Décision département : ",tcandidat[i]->TabVoeu[y]->ville,tcandidat[i]->TabVoeu[y]->dep);
                if(tcandidat[i]->TabVoeu[y]->ddep==0) printf("Dossier non traité\n");
                else
                {
                    if(tcandidat[i]->TabVoeu[y]->ddep==1) printf("Admis\n");
                    else
                    {
                        if (tcandidat[i]->TabVoeu[y]->ddep==2) printf("Liste d'attente\n");
                        else printf("Refusé\n");
                    }
                }
                printf("Décision du candidat : ");
                if(tcandidat[i]->TabVoeu[y]->vcand==0) printf("Décision non rendu\n");
                else
                {
                    if(tcandidat[i]->TabVoeu[y]->vcand==1) printf("Accepté\n");
                    else printf("Refusé");
                }
            }
            return;
        }
    }
    printf("Candidat non trouvé");
}

void ListeCandidat(int nbcandidat,candidat *tcandidat[])
{
    for(int i=0;i<nbcandidat;++i)
    {
        printf("Candidat n°%d :\n\
        Numéro : %d\n\
        Nom : %s\n\
        Prenom : %s\n\
        Note de mathématique : %f\n\
        Note de français : %f\n\
        Note d'anglais : %f\n\
        Note de spécialité : %f\n\
        Nombre de voeu : %d\n\
        Voeu :\n",i,tcandidat[i]->numcandid,tcandidat[i]->surname,tcandidat[i]->name,\
        tcandidat[i]->note[0],tcandidat[i]->note[1],tcandidat[i]->note[2],\
        tcandidat[i]->note[3],tcandidat[i]->nbchoix);
        for(int y=0;y<tcandidat[i]->nbchoix;++y)
        {
            printf("Ville : %s\n\
            Departement : %s\n\
            Décision département : ",tcandidat[i]->TabVoeu[y]->ville,tcandidat[i]->TabVoeu[y]->dep);
            if(tcandidat[i]->TabVoeu[y]->ddep==0) printf("Dossier non traité\n");
            else
            {
                if(tcandidat[i]->TabVoeu[y]->ddep==1) printf("Admis\n");
                else
                {
                    if (tcandidat[i]->TabVoeu[y]->ddep==2) printf("Liste d'attente\n");
                    else printf("Refusé\n");
                }
            }
            printf("Décision du candidat : ");
            if(tcandidat[i]->TabVoeu[y]->vcand==0) printf("Décision non rendu\n");
            else 
            {
                if(tcandidat[i]->TabVoeu[y]->vcand==1) printf("Accepté\n");
                else printf("Refusé");
            }
        }
        return;
    }
    
    printf("Candidat non trouvé");
}

void Candidater(VilleIUT* tiut[],int tlogi, candidat* tcandidat[], int nbcandidat)
{
    char VilleRech[31];
    printf("Veuillez entrez la ville de l'IUT au quel vous souhaitez candidatez : ");
    scanf("%s",VilleRech);
    int ind=rechercheIUT(tiut,tlogi,VilleRech);
    if(ind==-1) return;
    else
    {
        char DepRech[31];
        printf("Veuillez entrez le département dans lequel vous souhaitez y postuler : ");
        scanf("%s",DepRech);
        if (existeDep(tiut[ind]->ldept, DepRech)==0){
            fprintf(stderr,"Erreur, le département n'existe pas.");
            return;
        }
        else
        {
            int i, numcandid;
            printf("Veuillez entrez le numéro du candidat qui vous a été attribué : ");
            scanf("%d",&numcandid);
            for (i=0; i<nbcandidat && tcandidat[i]->numcandid!=numcandid;++i);
            voeu* voeu_ajout=(voeu*) malloc (sizeof(voeu));
            if(voeu_ajout==NULL){
                perror("malloc");
                exit(errno);
            }
            strcpy(voeu_ajout->ville,VilleRech);
            strcpy(voeu_ajout->dep,DepRech);
            voeu_ajout->ddep=0;
            voeu_ajout->vcand=0;
            printf("%d\n", tcandidat[i]->nbchoix);
            tcandidat[i]->TabVoeu[tcandidat[i]->nbchoix]=voeu_ajout;
            tcandidat[i]->nbchoix++;
        } 

    } 
}


void CreateCandidat(candidat** tcandid,int* nbcandidat)
{
    candidat* Candidat_Ajout=(candidat*)malloc(sizeof(candidat));
    if(Candidat_Ajout==NULL){
        perror("malloc");
        exit(errno);
    }
    printf("Merci de rentrez votre nom : ");
    scanf("%*c");
    fgets(Candidat_Ajout->name, 31, stdin);
    int i=0;
    while(Candidat_Ajout->name[i+1]!='\0')i++;
    Candidat_Ajout->name[i]='\0';
    printf("Merci de rentrez votre prénom : ");
    fgets(Candidat_Ajout->surname, 31, stdin);
    i=0;
    while(Candidat_Ajout->surname[i+1]!='\0')i++;
    Candidat_Ajout->surname[i]='\0';
    printf("Merci de rentrez votre note en Mathématique : ");
    scanf("%f",&Candidat_Ajout->note[0]);
    printf("Merci de rentrez votre note en Français : ");
    scanf("%f",&Candidat_Ajout->note[1]);
    printf("Merci de rentrez votre note en Anglais : ");
    scanf("%f",&Candidat_Ajout->note[2]);
    printf("Merci de rentrez votre note en Spécialité : ");
    scanf("%f",&Candidat_Ajout->note[3]);
    tcandid=(candidat**)realloc(tcandid,sizeof(candidat)*((*nbcandidat)+1));
    if (tcandid==NULL)
    {
        fprintf(stderr,"Tableau candidat corrompue\n");
        perror("Erreur :");
        exit(errno);
    }
    Candidat_Ajout->nbchoix=0;
    tcandid[*nbcandidat-1]=Candidat_Ajout;
    printf("Ajout fini\nVotre numéro de candidat est : %d",\
    Candidat_Ajout->numcandid);
    *nbcandidat+=1;
}