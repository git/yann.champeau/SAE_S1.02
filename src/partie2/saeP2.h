/**
*\file saeP2.h
*\brief Liste des fonctions utilisées dans la partie 2.
*\author Van Brabandt.J Champeau.Y
*\date 13 janvier 2023
*
*Fichier regroupant les prototypes des fonctions utilisées par l'application dans la partie 2 de la SAE.
*/



/**
 * \struct voeu
 * \brief Structure correspondant à une candidature pour un département.
*/
typedef struct
{
    char ville[31];
    char dep[31];
    int ddep;
    int vcand;
}voeu;



/**
 * \struct candidat
 * \brief Structure correspondant à un candidat.
*/
typedef struct
{
    int numcandid;
    char surname[31];
    char name[31];
    float note[4];
    int nbchoix;
    voeu* TabVoeu[20];
}candidat;



/**
* \brief fonction chargeant le fichier des IUTs. 
* \param [out] CandidOpen Entier contenant 1 si la phase de canditatures est ouverte, 0 sinon.
* \param [out] nbcandidat nombre de candidats enregistrés.
* \return le tableau de candidats.
*/
candidat** LoadCandid(int *CandidOpen,int* nbcandidat);
    /*
    Charge le fichier candidature
    Entrée : 
        Booléen Candidature par adresse pour lire la valeur dans le fichier CandidOpen;
        taille logique du tableau de candidat nbcandidat;
    Sortie : 
        tableau des candidats.
    */



/**
* \brief sauvegarde le tableau de candidats dans le fichier.
* \param [in] nbcandidat taille logique du tableau et nombre de candidats.
* \param [in] tcandidat Tableau contenant les candidats.
* \param [in] CandidOpen Entier contenant 1 si la phase de canditatures est ouverte, 0 sinon.
*/
void SaveCandid(int nbcandidat,candidat *tcandidat[],int CandidOpen);
    /*
    Sauvegarde le fichier candidature
    Entrée : 
        taille logique de tcandidat nbcandidat;
        tableau des candidats tcandidats. 
        Booléen gérant l'ouverture ou non des candidatures CandidOpen; 
    Sortie : Aucune
    */



/**
* \brief recherche et affiche un candidat dans le tableau des candidats.
* \param [in] ID identifiant du candidat à chercher.
* \param [in] nbcandidat taille logique du tableau et nombre de candidats.
* \param [in] tcandidat Tableau contenant les candidats.
*/
void RechercheCandidat(int ID, int nbcandidat,candidat* tcandidat[]);
    /*
    Recherche et affiche un candidat selon son ID
    Entrée : 
        ID du candidat ID;
        taille logique de tcandidat nbcandidat;
        tableau des candidats tcandidats. 
    Sortie : Aucune
    */



/**
* \brief affiche les candidats.
* \param [in] nbcandidat taille logique du tableau et nombre de candidats.
* \param [in] tcandidat Tableau contenant les candidats.
*/
void ListeCandidat(int nbcandidat,candidat *tcandidat[]);
    /*
    Liste tout les candidats.
    Entrée : 
        taille logique de tcandidat nbcandidat;
        tableau des candidats tcandidats. 
    Sortie : Aucune
    */



/**
* \brief permet de faire une candidature pour un département.
* \param [in] tiut Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
* \param [in,out] tcandidat Tableau contenant les candidats.
* \param [in] numcandid numéro du candidat à chercher.
*/
void Candidater(VilleIUT* tiut[],int tlogi, candidat* tcandidat[], int nbcandidat);
    /*
    Permet de candidater dans un IUT
    Entrée : 
        Tableau des IUT tiut;
        taille logique de tiut tlogi;
        Booléen gérant l'ouverture ou non des candidatures CandidOpen; 
        tableau des candidats tcandidats. 
    Sortie : Aucune
    */



/**
* \brief permet de créer un candidat.
* \param [in,out] tcandid Tableau contenant les candidats.
* \param [in] nbcandidat taille logique du tableau et nombre de candidats.
*/
void CreateCandidat(candidat** tcandid,int* nbcandidat);