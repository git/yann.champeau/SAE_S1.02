//Structure//

/**
*\file saeP1.h
*\brief Liste des fonctions utilisées dans la partie 1.
*\author Van Brabandt.J Champeau.Y
*\date 13 janvier 2023
*
*Fichier regroupant les prototypes des fonctions utilisées par l'application dans la partie 1 de la SAE.
*/


/**
 * \struct MaillonDep
 * \brief Structure correspondant à un département d'un IUT.
*/
typedef struct MaillonDep
{
    char departement[31];
    int nbp;
    char resp[51];
    struct MaillonDep* suivant;
} MaillonDep;



/**
 * \struct VilleIUT
 * \brief Structure correspondant à un IUT.
*/
typedef struct
{
    char Ville[31];
    MaillonDep* ldept;
} VilleIUT;

#include "../partie2/saeP2.h"



//Consultant//

/**
* \brief affiche le menu du compte Consultant.
* \param [in] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
* \param [in] CandidOpen Entier contenant 1 si la phase de canditatures est ouverte, 0 sinon.
* \param [in] tcandidat Tableau contenant les candidats.
* \param [in] nbcandidat taille logique du tableau et nombre de candidats.
*/
void menuUser(VilleIUT* lvIUT[],int tlogi, int CandidOpen, candidat* tcandidat[], int* nbcandidat);
    /*
    Menu pour les utilisateurs du système, les candidats.
    Entrée : 
        Tableau des IUT tiut;
        taille logique de tiut tlogi;
        Booléen gérant l'ouverture ou non des candidatures CandidOpen; 
        tableau des candidats tcandidats. 
    Sortie : Aucune
    */



/**
* \brief affiche les villes contenant un IUT.
* \param [in] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void SearchCityWithIUT(VilleIUT* lvIUT[], int tlogi);
    /*
    Affiche toute les villes possèdant un IUT
    Entrée : 
        Tableau des IUT tiut;
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



/**
* \brief affiche les villes contenant un IUT et leurs départements.
* \param [in] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void DepEachIUT(VilleIUT* lvIUT[], int tlogi);
    /*
    Affiche toutes les villes possédant un IUT et leurs départements
    Entrée : 
        Tableau des IUT tiut;
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



/**
* \brief affiche les places disponibles en première année dans un département d'un IUT.
* \param [in] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void SearchPlaceFromDepInIUT(VilleIUT* lvIUT[],int tlogi);
    /*
    Affiche le nombre de place d'un département précis dans un iut précis.
    Les deux sont entrés par l'utilisateur.
    Entrée : 
        Tableau des IUT tiut;
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



/**
* \brief affiche les IUTs contenant un département précis.
* \param [in] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void SearchIUTFromDep(VilleIUT* lvIUT[],int tlogi);
    /*
    Affiche toutes les villes possédant le département entré par l'utilisateur.
    Entrée : 
        Tableau des IUT tiut;
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



//Admin//

/**
* \brief affiche le menu du compte Administrateur.
* \param [in,out] tiut Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
* \param [in] CandidOpen Entier contenant 1 si la phase de canditatures est ouverte, 0 sinon.
*/
void menuAdmin(VilleIUT* tiut[],int* tlogi,int* CandidOpen);
    /*
    Menu pour les administrateurs du système.
    Entrée : 
        Tableau des IUT tiut; 
        taille logique de tiut tlogi;
        booléen indiquant l'ouverture ou fermeture des candidature CandidOpen;
    Sortie : Aucune
    */



/**
* \brief permet de modifier le nombre de places dans un département.
* \param [in,out] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void modifPlaces(VilleIUT* tiut[],int tlogi);
    /*
    Permet de modifier les places d'un département dans un IUT.
    Entrée : 
        Tableau des IUT tiut; 
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



/**
* \brief permet de créer un département dans un IUT.
* \param [in,out] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void creerDep(VilleIUT* tiut[],int tlogi);
    /*
    Permet de créer un département dans un IUT.
    Entrée : 
        Tableau des IUT tiut; 
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



/**
* \brief permet de supprimer un département dans un IUT.
* \param [in,out] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void supprimerDep(VilleIUT* tiut[],int tlogi);
    /*
    Permet de supprimer un département dans un IUT.
    Entrée : 
        Tableau des IUT tiut; 
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



/**
* \brief permet de modifier le nom du responsable d'un département.
* \param [in,out] lvIUT Tableau contenant les IUTs.
* \param [in] tlogi taille logique du tableau.
*/
void modifNomResponsable(VilleIUT* tiut[],int tlogi);
    /*
    Permet de modifier le nom d'un responsable d'un département d'un IUT.
    Entrée : 
        Tableau des IUT tiut; 
        taille logique de tiut tlogi;
    Sortie : Aucune
    */



/**
* \brief permet de créer un IUT.
* \param [in,out] lvIUT Tableau contenant les IUTs.
* \param [in,out] tlogi taille logique du tableau.
*/
void creerIUT(VilleIUT* tiut[],int* tlogi);
    /*
    Permet de créer un IUT (quand elle segfault pas.)
    Entrée : 
        Tableau des IUT tiut; 
        taille logique de tiut tlogi;
    Sortie : Aucune
    */