#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "saeP1.h"
#include "../annexe/saeAnnexe.h"

//Partie Consultant.

void menuUser(VilleIUT* lvIUT[],int tlogi, int CandidOpen, candidat* tcandidat[], int* nbcandidat)
{
    int quit=0, ck=0, act;
    while(!quit)
    {
    printf("#--------------------------------------------------------------------#\n");
    printf("|                                                                    |\n");
    printf("|                             SAE S1.02                              |\n");
    printf("|                                                                    |\n");
    printf("#--------------------------------------------------------------------#\n\n");
    printf("Codes correspondant aux différentes actions :\n\n");
    printf("1 : Rechercher les villes possèdant un IUT.\n");
    printf("2 : Afficher tout les départements de chaques IUT\n");
    printf("3 : Afficher le nombre de place en première année d'un IUT précis avec un département précis\n");
    printf("4 : Rechercher les IUTs possèdant un département précis.\n");
    printf("5 : S'enregistrer en tant que candidat.\n");
        if(CandidOpen)
        {
            printf("6 : Candidater\n");
        }
    printf("9 : Revenir à l'écran de sélection du profil.\n\n");
    printf("#--------------------------------------------------------------------#\n\n");
	ck=0;
    while(!ck)
	{
        printf("Choisissez l'action que vous voulez exécuter : ");
        if(scanf("%d",&act)) ck=1;
	}
    switch(act){
        case 1:
            SearchCityWithIUT(lvIUT,tlogi);
            reset();
            break;
        case 2:
            DepEachIUT(lvIUT,tlogi);
            reset();
            break;
        case 3:
            SearchPlaceFromDepInIUT(lvIUT,tlogi);
            reset();
            break;
        case 4:
            SearchIUTFromDep(lvIUT,tlogi);
            reset();
            break;
        case 5:
            CreateCandidat(tcandidat,nbcandidat);
            reset();
            break;
        case 6:
            if(CandidOpen) Candidater(lvIUT,tlogi,tcandidat,*nbcandidat);
            reset();
            break;
        case 9:
            quit=1;
            
         } 
    }
    candidat* tmp = (candidat*) malloc (sizeof(candidat));
    tcandidat[0]=tmp;
}

void SearchCityWithIUT(VilleIUT* lvIUT[], int tlogi)
{
    for (int i=0;i<tlogi;++i) printf("\n - %s",lvIUT[i]->Ville);
}

void DepEachIUT(VilleIUT* lvIUT[], int tlogi)
{
    for (int i=0;i<tlogi;++i)
    {
        printf("\n - %s :\n",lvIUT[i]->Ville);
        MaillonDep* MaillonAct=lvIUT[i]->ldept;
        while (MaillonAct!=NULL) 
        {
            printf("\t-%s\n",MaillonAct->departement);
            MaillonAct=MaillonAct->suivant;
        }
    }
}

void SearchPlaceFromDepInIUT(VilleIUT* lvIUT[],int tlogi)
{
    char sVille[31];
    char sDep[31];
    int boolF=0, i;
    printf("\nEntrez le nom de la ville contenant le département recherché :\n");
    scanf("%s",sVille);
    for (i=0;i<tlogi;++i)
    {
        if (!strcmp(lvIUT[i]->Ville,sVille))
        {
            printf("\nDépartement y étant disponible :\n");
            MaillonDep* MaillonAct=lvIUT[i]->ldept;
            while (MaillonAct!=NULL) 
            {
                printf("\t-%s\n",MaillonAct->departement);
                MaillonAct=MaillonAct->suivant;
            }
            boolF=1;
            break;
        }
    }
    if (!boolF)
    {
        printf("\nLa ville n'est pas dans la liste.\nMerci de vérifier l'orthographe.\n");
        return;
    }
    printf("\nEntrez le nom du département dont vous souhaitez connaitre le nombre de place\n");
    scanf("%s",sDep);
    MaillonDep* MaillonAct=lvIUT[i]->ldept;
    while (MaillonAct!=NULL) 
    {
        
        if(!strcmp(MaillonAct->departement,sDep))
        {
            printf("\nNombre de place : %d\n",MaillonAct->nbp);
            return;
        }
        MaillonAct=MaillonAct->suivant;
    }
    printf("\nLe département n'est pas dans la liste.\nMerci de vérifier l'orthographe.\n");
}

void SearchIUTFromDep(VilleIUT* lvIUT[],int tlogi)
{
    char sDep[31];
    printf("\nMerci de renseignez le département dont vous cherchez les IUTs :\n");
    scanf("%s",sDep);
    printf("\nVoici la liste des IUTs contenant le département mentionné :\n\n");
    for (int i=0;i<tlogi;++i)
    {
        MaillonDep* MaillonAct=lvIUT[i]->ldept;
		while(MaillonAct!=NULL){
			if(!strcmp(MaillonAct->departement,sDep)) printf("\t-%s\n",lvIUT[i]->Ville);
			MaillonAct=MaillonAct->suivant;
		}
    }
}

//Partie Administrateur.

void menuAdmin(VilleIUT* tiut[],int *tlogi, int* CandidOpen){
	int quit=0, ck=0, act;
	while(!quit){
		printf("#--------------------------------------------------------------------#\n");
		printf("|                                                                    |\n");
		printf("|                             SAE S1.02                              |\n");
		printf("|                                                                    |\n");
		printf("#--------------------------------------------------------------------#\n\n");
		printf("Codes correspondant aux différentes actions :\n\n");
		printf("1 : Modifier le nombre de places dans un département.\n");
		printf("2 : Créer un département dans un IUT.\n");
		printf("3 : Supprimer un département d’un IUT.\n");
        if (*CandidOpen) printf("4 : Arrêter la phase de candidature.\n");
        else printf("4 : Lancer la phase de candidature.\n");
		printf("5 : Modifier le nom d'un responsable de département.\n");
		printf("6 : Créer un IUT.\n");
		printf("9 : Revenir à l'écran de sélection du profil.\n\n");
		printf("#--------------------------------------------------------------------#\n\n");
		ck=0;
        while(!ck)
		{
            printf("Choisissez l'action que vous voulez exécuter : ");
            if(scanf("%d",&act)) ck=1;
		}
		switch(act){
		    case 1:
                modifPlaces(tiut,*tlogi);
                reset();
                break;
		    case 2:
                creerDep(tiut,*tlogi);
                reset();
                break;
		    case 3:
                supprimerDep(tiut,*tlogi);
                reset();
                break;
		    case 4:
                if(!*CandidOpen)
                {
                    printf("Ouverture de la phase de candidature effectué !");
                    *CandidOpen=1;
                }
                else
                {
                    printf("Fermeture de la phase de candidature effectué !");
                    *CandidOpen=0;
                }
                reset();
                break;
		    case 5:
                modifNomResponsable(tiut,*tlogi);
                reset();
                break;
		    case 6:
                creerIUT(tiut,tlogi);
                reset();
                break;
		    case 9:
                quit=1;
		}
	}
}

void modifPlaces(VilleIUT* tiut[],int tlogi){
    printf("\nEntrez la ville correspondant à l'IUT à modifier (Q pour abandonner): ");
    char ville[31];
    scanf("%s",ville);
    if(ville[0]=='Q' && ville[1]=='\0') return;
    int noVille=rechercheIUT(tiut,tlogi,ville);
    if(noVille<0){
        fprintf(stderr,"\nVille non existante !\n");
        return;
    }
    VilleIUT* v=tiut[noVille];
    printf("\n\nEntrez le département à modifier : ");
    char dep[31];
    scanf("%s",dep);
    if(!existeDep(v->ldept,dep)){
        fprintf(stderr,"\nDépartement non existant !\n");
        return;
    }
    MaillonDep* m=v->ldept;
    while(m->departement!=dep && m->suivant!=NULL) m=m->suivant;
    printf("\n\nIl y a actuellement %d place(s) dans le département. A combien voulez-vous modifier ce nombre ?\n\n",m->nbp);
    int nPlaces;
    scanf("%d",&nPlaces);
    m->nbp=nPlaces;
    printf("\n\nLe nombre de places dans le département %s de l'IUT de %s a bien été modifié à %d place(s).\n",dep,ville,nPlaces);
    return;
}

void creerDep(VilleIUT* tiut[],int tlogi){
    printf("\nEntrez la ville correspondant à l'IUT à modifier (Q pour abandonner): ");
    char ville[31];
    scanf("%s",ville);
    if(ville[0]=='Q' && ville[1]=='\0') return;
    int noVille=rechercheIUT(tiut,tlogi,ville);
    if(noVille<0){
        fprintf(stderr,"\nVille non existante !\n");
        return;
    }
    VilleIUT* v=tiut[noVille];
    int ck=0;
    if(v->ldept==NULL){
    	ck=1;
    	v->ldept=(MaillonDep*)malloc(sizeof(MaillonDep));
    	if(v->ldept==NULL){
        	perror("malloc");
        	exit(errno);
    	}
    }
	MaillonDep* m=v->ldept;
    printf("\n\nEntrez le nom du département à créer : ");
    char nomDep[31];
    scanf("%*c%s",nomDep);
	if(!ck){
	    if(m->suivant==NULL){
	        if(strcmp(nomDep,m->departement)>0){
	            m=m->suivant;
	        }
	    }
		while(strcmp(nomDep,m->departement)>0) m=m->suivant;
		m->suivant=(MaillonDep*)malloc(sizeof(MaillonDep));
		if(m->suivant==NULL){
			perror("malloc");
			exit(errno);
		}
		m=m->suivant;
	}
	strcpy(m->departement,nomDep);
    printf("\n\nEntrez le nombre de places disponibles dans le département : ");
    scanf("%d",&m->nbp);
    printf("\n\nEntrez le nom du responsable de département : \n");
    scanf("%*c");
    fgets(m->resp, 51, stdin);
    int i=0;
    while(m->resp[i+1]!='\0')i++;
    m->resp[i]='\0';
    printf("\n\nLe département %s a bien été ajouté à l'IUT de %s.\n",m->departement,ville);
    return;
}

void supprimerDep(VilleIUT* tiut[],int tlogi){
    printf("\nEntrez la ville correspondant à l'IUT à modifier (Q pour abandonner): ");
    char ville[31];
    scanf("%s",ville);
    if(ville[0]=='Q' && ville[1]=='\0') return;
    int noVille=rechercheIUT(tiut,tlogi,ville);
    if(noVille<0){
        fprintf(stderr,"\nVille non existante !\n");
        return;
    }
    VilleIUT* v=tiut[noVille];
    printf("\n\nEntrez le département à supprimer:");
    char dep[31];
    scanf("%s",dep);
    if(!existeDep(v->ldept,dep)){
        fprintf(stderr,"\nDépartement non existant !\n");
        return;
    }
    MaillonDep* m=v->ldept;
    if(m->departement==dep){
        printf("\n\nVoulez-vous supprimer le département %s de la ville de %s ? (Y pour continuer)\n\n",m->departement,v->Ville);
        char choix;
        scanf("%*c%c",&choix);
        if(choix!='Y') return;
        v->ldept=m->suivant;
        free(m);
    }
    else{
        while(m->suivant->departement!=dep) m=m->suivant;
        printf("\n\nVoulez-vous supprimer le département %s de la ville de %s ? (Y pour continuer)\n\n",m->suivant->departement,v->Ville);
        char choix;
        scanf("%*c%c",&choix);
        if(choix!='Y') return;
        MaillonDep* temp;
        temp=m->suivant;
        m->suivant=m->suivant->suivant;
        free(temp);
    }
    printf("\n\nLe département %s de l'IUT de %s a bien été supprimé.\n",dep,ville);
    return;
}

void modifNomResponsable(VilleIUT* tiut[],int tlogi){
    printf("\nEntrez la ville correspondant à l'IUT à modifier (Q pour abandonner): ");
    char ville[31];
    scanf("%s",ville);
    if(ville[0]=='Q' && ville[1]=='\0') return;
    int noVille=rechercheIUT(tiut,tlogi,ville);
    if(noVille<0){
        fprintf(stderr,"\nVille non existante !\n");
        return;
    }
    VilleIUT* v=tiut[noVille];
    printf("\n\nEntrez le département à modifier :");
    char dep[31];
    scanf("%*c%s",dep);
    if(!existeDep(v->ldept,dep)){
        fprintf(stderr,"\nDépartement non existant !\n");
        return;
    }
    MaillonDep* m=v->ldept;
    while(m->departement!=dep && m->suivant!=NULL) m=m->suivant;
    printf("\n\nLe responsable du département %s de l'IUT de la ville de %s est actuellement %s.\nQuel est le nom du nouveau responsable ?\n\n",m->departement,v->Ville,m->resp);
    scanf("%*c");
    fgets(m->resp, 51, stdin);
    int i=0;
    while(m->resp[i+1]!='\0')i++;
    m->resp[i]='\0';
    printf("\n\nLe nom du responsable du département %s de l'IUT de %s a bien été changé pour %s.\n",dep,ville,m->resp);
    return;
}

void creerIUT(VilleIUT* tiut[],int* tlogi){
	printf("\nEntrez le nom de la ville correspondant à l'IUT à ajouter (Q pour abandonner): ");
	char ville[31];
	scanf("%*c%s",ville);
	if(ville[0]=='Q' && ville[1]=='\0') return;
	int noVille=rechercheIUT(tiut,*tlogi,ville);
	if(noVille>=0){
		fprintf(stderr,"\nVille déjà existante !\n");
		return;
	}
	VilleIUT* v=(VilleIUT*)malloc(sizeof(VilleIUT));
	if(v==NULL){
        	perror("malloc");
        	exit(errno);
    	}
	strcpy(v->Ville,ville);
	v->ldept=NULL;
	tiut[*tlogi]=v;
	*tlogi=*tlogi+1;
	printf("\n\nL'IUT de %s a bien été ajouté.\n",ville);
	return;
}
