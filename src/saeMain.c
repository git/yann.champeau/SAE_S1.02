#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "partie1/saeP1.h"
#include "annexe/saeAnnexe.h"
#include "partie3/saeP3.h"
#include "partie4/saeP4.h"

int main(void)
{
	system("clear");
	VilleIUT* tiut[36529];
	int CandidOpen=0;
	int nbcandidat=0;
	candidat** tcandidat=LoadCandid(&CandidOpen,&nbcandidat);
	int tlogi=loadIUT(tiut), quitter=0;
	while(!quitter){
		menu(CandidOpen);
		char user;
		scanf("%c",&user);
		switch(user){
			case 'C':
				reset();
				menuUser(tiut,tlogi,CandidOpen,tcandidat,&nbcandidat);
				reset();
				break;
			case 'A':
				reset();
				menuAdmin(tiut,&tlogi,&CandidOpen);
				reset();
				break;
			case 'S':
				saveIUT(tiut,tlogi);
				SaveCandid(nbcandidat,tcandidat,CandidOpen);
				reset();
				break;
			case 'R':
				reset();
				if (CandidOpen) menuResponsable(tiut,&tlogi,tcandidat);
				reset();
				break;
			case 'Q':
				printf("Êtes-vous sûr de vouloir quitter ? (Y pour continuer)\n\n");
				char confirmation;
				scanf("%*c%c",&confirmation);
				if(confirmation=='Y') quitter=1;
				system("clear");
		}
	}
	saveIUT(tiut,tlogi);
	SaveCandid(nbcandidat,tcandidat,CandidOpen);
	printf("L'application se ferme ...\n");
	return 0;
}
