/**
*\file saeP3.h
*\brief Liste des fonctions utilisées dans la partie 3.
*\author Van Brabant.J Champeau.Y
*\date 13 janvier 2023
*
*Fichier regroupant les prototypes des fonctions utilisées par l'application dans la partie 3 de la SAE.
*/


/**
* \brief Menu pour les reponsables de départements
* \param [in] tiut tableau des IUTs.   
* \param [in] tlogi taille logique de tiut tlogi;
* \param [in] tcandidat tableau des candidats tcandidats. 
* \return le tableau de candidats.
*/
void menuResponsable(VilleIUT* tiut[],int *tlogi,candidat* tcandidat[]);
    /*
    Menu pour les reponsables de départements
    Entrée : 
        tableau des IUTs tiut;
        taille logique de tiut tlogi;
        tableau des candidats tcandidats. 
    Sortie : Aucune
    */ 

/**
* \brief Recherche l'ID du voeu en IUT Informatique à Clermont Ferrand pour un candidat et la renvoie.
* \param [in] candid Candidat utilisé pour la recherche  
* \param [out] ID du voeu dans le tableau   
* \return le tableau de candidats.
*/
int RechercheVoeuIDInformatique(candidat candid);


/**
* \brief Affiche toutes les candidatures.
* \param [in] tcandidat tableau des candidats. 
* \param [in] nbcandidat taille logique de tcandidat.
* \return le tableau de candidats.
*/
void ShowCandid(candidat* tcandidat[],int tlogi);
    /*
    Affiche toutes les candidatures.
    Entrée : 
        tableau des candidats tcandidats. 
        taille logique de tcandidat nbcandidat;
    Sortie : Aucune
    */

/**
* \brief Gère l'entiéreté des fonctions d'admissions
* \param [in] tcandidat tableau des candidats. 
* \param [in] tlogi taille logique de tcandidat.
* \return le tableau de candidats.
*/
void Admissions(candidat* tcandidat[],int tlogi);
    /*
    Gère l'entiéreté des fonctions d'admissions.
    Entrée : 
        tableau des candidats tcandidats. 
        taille logique de tcandidat nbcandidat;
    Sortie : Aucune
    */


/**
* \brief Fusionne deux tableaux
* \param [in] tabfusion1 Premier tableau à fusioner. 
* \param [in] tlogi1 taille logique du premier tableau.
* \param [in] tabfusion2 Deuxième tableau à fusioner. 
* \param [in] tlogi2 taille logique du deuxième tableau.
* \return le tableau de candidats.
*/
//int* fusion(int* tabfusion1,int tlogi1, int* tabfusion2, int tlogi2);
int* fusion(int tabG[],int tailleG, int tabNoteG[], int tabD[], int tailleD, int tabNoteD[], int retNote[]);


/**
* \brief fonction chargeant le fichier des IUTs. 
* \param [in] tabNote Tableau de Note calculé avec les coefficients
* \param [in] tabIDCandidatAttente Tableau de candidats en liste d'attente.
* \param [in] taille Taille des tableaux
* \return le tableau de candidats.
*/

int* tri_fusion_rec(int tabIDCandidatAttente[], int tabNote[], int taille);

int* tri_fusion(int tabIDCandidatAttente[], int tabNote[], int taille);