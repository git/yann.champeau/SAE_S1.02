#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../partie1/saeP1.h"
#include "../annexe/saeAnnexe.h"
#include "saeP3.h"

//Partie Responsable.

void menuResponsable(VilleIUT* tiut[],int *tlogi,candidat* tcandidat[]){
	int quit=0, act;
	while(!quit){
		printf("#--------------------------------------------------------------------#\n");
		printf("|                                                                    |\n");
		printf("|                             SAE S1.02                              |\n");
		printf("|                                                                    |\n");
		printf("#--------------------------------------------------------------------#\n\n");
		printf("Codes correspondant aux différentes actions :\n\n");
		printf("1 : Voir toutes les candidatures du département informatique de Clermont-Ferrand\n");
		printf("2 : Procéder aux admissions\n\n");
		printf("9 : Quitter\n");
		printf("#--------------------------------------------------------------------#\n\n");
		int ck=0;
		while(!ck)
		{
			printf("Choisissez l'action que vous voulez exécuter : ");
			if(scanf("%d",&act)) ck=1;
		}
		switch(act){
			case 1:
                ShowCandid(tcandidat,*tlogi);
                reset();
                break;
		    case 2:
                Admissions(tcandidat,*tlogi);
                reset();
                break;
		    case 9:
                quit=1;
		}
	}
}

int RechercheVoeuIDInformatique(candidat candid)
{
	int ID_found=0;
	for(int i=0;i<candid.nbchoix && !ID_found;++i)
	{
		printf("T");
		if(!strcmp(candid.TabVoeu[i]->dep,"Informatique")) ID_found=i;
	}
	return ID_found;
}

void ShowCandid(candidat* tcandidat[],int tlogi)
{
	for(int i=0;i<tlogi;++i)
	{
		int voeuID;
		if((voeuID=RechercheVoeuIDInformatique(*tcandidat[i])))
		{
			printf("Nom - Prenom : %s %s\n\
            Note de mathématique : %f\n\
            Note de français : %f\n\
            Note d'anglais : %f\n\
            Note de spécialité : %f\n\n",tcandidat[i]->surname,tcandidat[i]->name,\
            tcandidat[i]->note[0],tcandidat[i]->note[1],tcandidat[i]->note[2],\
            tcandidat[i]->note[3]);
			printf("Décision département : %d\n\
			Décision candidat : %d\n\n\n\n",tcandidat[i]->TabVoeu[voeuID]->ddep,tcandidat[i]->TabVoeu[voeuID]->vcand);
		}
	}
}

void Admissions(candidat* tcandidat[],int tlogi)
{
	int ck=0;
	//Coef Note
	float coefMath, coefFr, coefEng, coefSpe;
	while (!ck)
	{
		printf("Merci de rentrer dans l'ordre et séparés par un espace les coefficients suivant : Mathématique Français Anglais Spécialité.\n\n");
		scanf("%f %f %f %f",&coefMath,&coefFr,&coefEng,&coefSpe);
	}
	//Note min
	ck=0;
	float minNote;
	while (!ck)
	{
		printf("Merci de renseignez maintenant la note minimum des candidats acceptés.\n\n");
		scanf("%f",&minNote);
	}
	//Nb admissions max
	int nbAdmisMax;
	ck=0;
	while (!ck)
	{
		printf("Veuillez maintenant rentrer le nombre d'admis maximum");
		scanf("%d",&nbAdmisMax);
	}
	//Calcul
	float tabNoteAdmissions[tlogi];
	for(int i=0;i<tlogi;++i)
	{
		float NoteAdmissions=0;
		NoteAdmissions+=tcandidat[i]->note[0]*coefMath;
		NoteAdmissions+=tcandidat[i]->note[1]*coefMath;
		NoteAdmissions+=tcandidat[i]->note[2]*coefMath;
		NoteAdmissions+=tcandidat[i]->note[3]*coefMath;
		tabNoteAdmissions[i]=NoteAdmissions;
	}
	//Admissions
	int admis=0;
	int attente=0;
	int tabIDCandidatAdmis[tlogi];
	int tabIDCandidatAttente[tlogi];
	int tabNoteCandidatAttente[tlogi];
	for(int i=0;i<tlogi && admis<=nbAdmisMax;++i)
	{
		int voeuID;
		if(tabNoteAdmissions[i]>=minNote){
		//Admis
			if(admis<nbAdmisMax)
			{
				tabIDCandidatAdmis[admis]=tcandidat[i]->numcandid;
				admis++;
				voeuID=RechercheVoeuIDInformatique(*tcandidat[i]);
				tcandidat[i]->TabVoeu[voeuID]->ddep=1;
			}
		//Attente
			else 
			{
				tabIDCandidatAttente[i-admis]=tcandidat[i]->numcandid;
				tabNoteCandidatAttente[i-admis]=tabNoteAdmissions[i];
				attente++;
				voeuID=RechercheVoeuIDInformatique(*tcandidat[i]);
				tcandidat[i]->TabVoeu[voeuID]->ddep=2;
			}
		}
		//Refus	
		else
		{
			voeuID=RechercheVoeuIDInformatique(*tcandidat[i]);
			tcandidat[i]->TabVoeu[voeuID]->ddep=-1;
		}
	}
	//Tri
	tri_fusion(tabIDCandidatAttente,tabNoteCandidatAttente,attente);
	//Save Admis liste
	FILE *fadmis;
	if((fadmis=fopen("src/Database/ListeAdmisInfo.bin","wb"))==NULL)
	{
		perror("Erreur :");
		exit(errno);
	}
	else 
	{
		fwrite(&admis,sizeof(int),1,fadmis);
		fwrite(tabIDCandidatAdmis,sizeof(int),admis,fadmis);
	}
	//Save Attente liste
	FILE *fattente;
	if((fattente=fopen("src/Database/ListeAttenteInfo.bin","wb"))==NULL)
	{
		perror("Erreur :");
		exit(errno);
	}
	else 
	{
		fwrite(&attente,sizeof(int),1,fadmis);
		fwrite(tabIDCandidatAttente,sizeof(int),attente,fattente);
	}
}

int* fusion(int tabG[],int tailleG, int tabNoteG[], int tabD[], int tailleD, int tabNoteD[], int retNote[]){
	int ret[tailleG+tailleD];
	int iG=0, iD=0;
	while (iG<tailleG&&iD<tailleD){
		if(tabNoteG[iG]<=tabNoteD[iD]){
			retNote[iG+iD]=tabNoteG[iG];
			ret[iG+iD]=tabG[iG];
			iG++;
		}
		else{
			retNote[iG+iD]=tabNoteD[iD];
			ret[iG+iD]=tabD[iD];
			iD++;
		}
	}
	if (iG==tailleG){
		while(iD<tailleD){
			retNote[iG+iD]=tabNoteD[iD];
			ret[iG+iD]=tabD[iD];
			iD++;
		}
	}
	if (iD==tailleD){
		while(iG<tailleG){
			retNote[iG+iD]=tabNoteG[iG];
			ret[iG+iD]=tabG[iG];
			iG++;
		}
	}
	return ret;
}

int* tri_fusion_rec(int tabIDCandidatAttente[], int tabNote[], int taille){
	if(taille==1) return tabIDCandidatAttente;
	
	int tabNoteG[taille/2], tabNoteD[taille-taille/2], retNote[taille];
	tabcpy(tabNote,tabNoteG,taille/2);
	tabcpy(tabNote+taille/2,tabNoteD,taille);
	int ret[taille];
	tabcpy(fusion(tri_fusion_rec(tabIDCandidatAttente,tabNoteG,taille/2),taille/2,tabNoteG,\
		tri_fusion_rec(tabIDCandidatAttente+taille/2,tabNoteD,taille-taille/2),taille-taille/2,tabNoteD,retNote),ret,taille);
	tabcpy(retNote,tabNote,taille);
	return ret;
}

int* tri_fusion(int tabIDCandidatAttente[], int tabNote[], int taille){
	int notes[taille];
	tabcpy(tabNote,notes,taille);
	return tri_fusion_rec(tabIDCandidatAttente,notes,taille);
}