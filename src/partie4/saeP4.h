/**
*\file saeP4.h
*\brief Liste des fonctions utilisées dans la partie 4.
*\author Van Brabant.J Champeau.Y
*\date 13 janvier 2023
*
*Fichier regroupant les prototypes des fonctions utilisées par l'application dans la partie 4 de la SAE.
*/


/**
* \brief Affiche la liste de Voeux validés
* \param [in] candid Candidat voulant la liste de ses voeux validés   
*/
void ListeVoeuValide(candidat candid);


/**
* \brief Permet au candidat de valider les décisions de départements
* \param [in] candid Candidat voulant la liste de ses voeux validés   
*/
void Validation(candidat candid);


/**
* \brief Recherche de manière Dichotomique un candidat dans la liste trié par ordre alphabétique
* \param [in] tCandidAdmis Tableau des candidatsAdmis
* \param [in] tlogi Taille logique du tableau TcandidAdmis
* \return ID du candidat dans le tableau
*/
void AffichageAdmisAlphabetique(candidat* tCandidAdmis,int tlogi);
    /*
    Affiche les candidats admis dans l'ordre alphabétique.
    Entrée : 
        tableau des candidats tCandidAdmis. 
        taille logique de tcandidAdmis tlogi;
    Sortie : Aucune
    */
