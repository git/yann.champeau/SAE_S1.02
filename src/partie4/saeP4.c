#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../partie1/saeP1.h"
#include "../annexe/saeAnnexe.h"
#include "../partie3/saeP3.h"
#include "saeP4.h"

void ListeVoeuValide(candidat candid)
{
    for(int i=0;i<candid.nbchoix;++i)
    {
        if(candid.TabVoeu[i]->ddep==1)
        {
            printf("Candidature pour %s %s accepté.\n",\
            candid.TabVoeu[i]->ville,candid.TabVoeu[i]->dep);
        }
        else 
        {
            if(candid.TabVoeu[i]->ddep==2)
            {
                printf("Candidature pour %s %s mise en attente.\n",\
                candid.TabVoeu[i]->ville,candid.TabVoeu[i]->dep);
            }
            else
            {
                if(candid.TabVoeu[i]->ddep==-1)
                {
                    printf("Candidature pour %s %s refusé.\n",\
                    candid.TabVoeu[i]->ville,candid.TabVoeu[i]->dep);
                }
                else printf("Candidature pour %s %s non traité.\n",\
                candid.TabVoeu[i]->ville,candid.TabVoeu[i]->dep);
            }
        }
    }
}

void Validation(candidat candid)
{
    printf("Merci de rentrez le numéro associé au voeu que vous souhaitez accepté");
    for(int i=0;i<candid.nbchoix;++i)
    {
        if(candid.TabVoeu[i]->ddep==1)
        {
            printf("%d %s %s\n",\
            i+1,candid.TabVoeu[i]->ville,candid.TabVoeu[i]->dep);
        }
    }
    int id_Choix;
    scanf("%d",&id_Choix);
    id_Choix-=1;
    for(int i=0;i<candid.nbchoix;++i)
    {
        if(i!=id_Choix&&candid.TabVoeu[i]->ddep==1)
        {
            candid.TabVoeu[i]->vcand=-1;
        }
        else
        {
            if (i==id_Choix&&candid.TabVoeu[i]->ddep==1)
            {
                candid.TabVoeu[i]->vcand=1;
            }
        }
    }
}

void AffichageAdmisAlphabetique(candidat* tCandidAdmis,int tlogi)
{
    for(int i=0;i<tlogi;++i)
    {
            printf("\
            Numéro : %d\n\
            Nom : %s\n\
            Prenom : %s\n\
            Note de mathématique : %f\n\
            Note de français : %f\n\
            Note d'anglais : %f\n\
            Note de spécialité : %f\n",tCandidAdmis[i].numcandid,tCandidAdmis[i].surname,tCandidAdmis[i].name,\
            tCandidAdmis[i].note[0],tCandidAdmis[i].note[1],tCandidAdmis[i].note[2],\
            tCandidAdmis[i].note[3]
            );
    }
}