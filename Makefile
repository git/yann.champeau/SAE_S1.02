.PHONY: clean doc all

all : prog EXE
	
prog : 
	make -C src all

EXE :
	rm -f SAE
	gcc -g -o SAE src/saeMain.o src/annexe/saeAnnexe.o src/partie1/saeP1.o src/partie2/saeP2.o src/partie3/saeP3.o src/partie4/saeP4.o

doc :
	make -C src doc

clean :
	make -C src clean
